#!/bin/bash
function instalar_repositorio_epel(){
    echo "Instalando EPEL"
    dnf install -y epel-release
    crb enable
    echo "Verificando se o EPEL foi instalado"
    cat -n /etc/yum.repos.d/Rocky-PowerTools.repo
}

function instalar_dependencias_comuns(){
    #como o pacote pkg-install  nunca é encontrado removi da lista de pacotes a ser instalado 
    yum install -y gnupg chkconfig python3-pip git 
}

#No tutorial da ferramenta instala uma versão mais configura outra

JAVA_HOME=/usr/lib/jvm/java-11-openjdk
function instalar_java11(){
    echo "Instalando java 11"
    sudo dnf install -y java-11-openjdk-headless.x86_64
    echo JAVA_HOME="/usr/lib/jvm/java-11-openjdk" | sudo tee -a /etc/environment
    export JAVA_HOME="/usr/lib/jvm/java-11-openjdk"
}

REPO_ELASTICSEARCH_FILE=/etc/yum.repos.d/elasticsearch.repo
REPO_ELASTICSEARCH_TEXT="[elasticsearch]
name=Elasticsearch repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=0
autorefresh=1
type=rpm-md"         
ELASTICSEARCH_YML_TEXT='http.host: 127.0.0.1
transport.host: 127.0.0.1
cluster.name: hive
thread_pool.search.queue_size: 100000
path.logs: "/var/log/elasticsearch"
path.data: "/var/lib/elasticsearch"
xpack.security.enabled: false
script.allowed_types: "inline,stored"'
ELASTICSEARCH_YML_FILE=/etc/elasticsearch/elasticsearch.yml
JVM_OPTIONS_FILE=/etc/elasticsearch/jvm.options.d/jvm.options
JVM_OPTIONS_TEXT="-Dlog4j2.formatMsgNoLookups=true
-XmsMEMORIAg
-XmxMEMORIAg"
function instalar_elasticsearch7(){
    echo "Configurando o repositório" 
    echo "$REPO_ELASTICSEARCH_TEXT" > $REPO_ELASTICSEARCH_FILE
    echo "Instalando elasticsearch"
    sudo yum install -y --enablerepo=elasticsearch elasticsearch
    echo "Configurando o elasticsearch"  
    echo "$ELASTICSEARCH_YML_TEXT" > $ELASTICSEARCH_YML_FILE
    echo "Elasticsearch configurado"
    echo "Configurando o elasticsearch"  
    read -p 'Informe quatos giga de memória deseja destinar ao elasticsearch: ' MEMORIA
    echo "Você escolheu ${MEMORIA}G de memória, confira o arquivo e edite caso necessário: "
    echo "${JVM_OPTIONS_TEXT//MEMORIA/$MEMORIA}" > $JVM_OPTIONS_FILE
    nano $JVM_OPTIONS_FILE
    echo "Elasticsearch configurado"
    echo "Configurando as permições da pasta /etc/elasticsearch"
    chown -R elasticsearch:elasticsearch /etc/elasticsearch
    echo "verificando as permições da pasta /etc/elasticsearch"
    ls -la /etc/elasticsearch
    echo "Iniciando o elasticsearch"
    systemctl start elasticsearch.service
    echo "Verificando se o elasticsearch está iniciado"
    systemctl status elasticsearch.service
}

function remover_versoes_antigas_do_docker(){
    echo "Removendo versões antigas do Docker Engine"
    sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine \
                  podman \
                  runc
}

function instalar_docker(){
    echo "Instalando o yum-utils, pacote que disponibiliza o yum-config-manager"
    sudo yum install -y yum-utils
    echo "Instalando o repositório do docker"
    sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    echo "Instalando o docker"
    sudo yum install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    echo "Iniciando o docker"
    sudo systemctl start docker
    echo verificando se o docker foi iniciado
    sudo systemctl status docker.service
}

CORTEX_FILE=/etc/yum.repos.d/thehive-project.repo
CORTEX_TEXT="[cortex]
enabled=1
priority=1
name=TheHive-Project RPM repository
baseurl=https://rpm.thehive-project.org/release/noarch
gpgkey=https://raw.githubusercontent.com/TheHive-Project/Cortex/master/PGP-PUBLIC-KEY
gpgcheck=1"

echo "Verificando a existência do $CORTEX_FILE"           
function instalar_cortex(){
    echo "Configurando o repositório do cortex"  
    echo "$CORTEX_TEXT" > $CORTEX_FILE
    echo "Repositório do cortex cnofigurado"
    echo "Instalando o cortex"
    sudo yum install -y cortex
    echo "cortex instalado"
    echo "Adicionando o usuário do cortex ao grupo do docker"
    sudo usermod -aG docker cortex
}

function configurando_secretkey_cortex(){
    echo "Configurando secret key" 
    echo "Gerando arquivo secret.conf"
    echo play.http.secret.key=\""$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)"\" >> /etc/cortex/secret.conf
    echo "Verificando a criação do arquivo secret.conf"
    nano /etc/cortex/secret.conf 
    echo "Alterando a application.conf"
    sed -i 's/#play.http.secret.key=\"\*\*\*CHANGEME\*\*\*\"/include "\/etc\/cortex\/secret.conf"/' /etc/cortex/application.conf
    echo "Verificando se o arquivo foi realmente alterado" 
    less /etc/cortex/application.conf
    echo "Corrigindo as permissões da pasta /etc/cortex"
    chown -R cortex:cortex /etc/cortex
    echo "Verificando as pastas do cortex"
    ls -la /etc/cortex
}  

function configurar_analizers_e_responders(){
    echo "Instalando dependências do Analyzers e Responders"
    #removi o --no-install-recommends
    sudo yum install -y python3-pip python3-devel ssdeep git
    echo "Equivalente ao build-essential"
    sudo yum install -y gcc gcc-c++ kernel-devel make 
    echo "Instalando equivalente ao libmagic"
    sudo yum install -y file-devel
    pip3 install -U pip setuptools 
    #yum  install -y  python3-pip python3-dev ssdeep libfuzzy-dev libfuzzy2 libimage-exiftool-perl libmagic1 build-essential libssl-dev
    cd /opt
    git clone https://github.com/TheHive-Project/Cortex-Analyzers
    chown -R cortex:cortex /opt/Cortex-Analyzers 
    for I in $(find Cortex-Analyzers -name 'requirements.txt'); do sudo -H pip3 install -r $I || true; done

    echo "Configurando path dos Analyzers e Responders no /etc/cortex/application.conf"
    sed -i 's/ #"\/absolute\/path\/of\/analyzers"/ \/opt\/Cortex-Analyzers\/analyzers/' /etc/cortex/application.conf
    sed -i 's/ #"\/absolute\/path\/of\/responders"/ \/opt\/Cortex-Analyzers\/responders/' /etc/cortex/application.conf
}

function iniciar_service_cortex(){
    echo "Iniciando o cortex"
    systemctl start cortex.service
    echo "Configurando cortex para iniciar junto com o sistema"
    systemctl enable cortex.service
    echo "Verificando se o cortex foi iniciado"
    systemctl status cortex.service
}

function abrir_porta_no_firewall(){
    echo "Configurando porta do cortex no firewalld"
    sudo firewall-cmd --permanent --zone=public --add-port=9001/tcp
}

function verificar_servicos(){
    echo "Verificando portas abertas"
    ss -nltp
    echo "Serviços iniciando junto com o sistema: "
    systemctl list-unit-files --type=service --state=enabled
}

function configurar_nftables(){
    #usado caso firewalld continue apresentando falhas
    echo "Movendo  rules-cortex.nft para a pasta /etc/nftables/"
    mv rules-cortex.nft /etc/nftables/rulesets
    echo 'include "/etc/nftables/rulesets/rules-cortex.nft"' >> /etc/sysconfig/nftables.conf
    echo "Reiniciando o nftables"
    systemctl restart nftables
    echo "Habilitando o nftables para iniciar junto com o sistema"
    systemctl enable nftables.service
}

function verificar_usuario_correto(){
    USER=$(whoami)
    if [ "$USER" != "root" ]
    then
        echo "Você não esta logado como usuário root, abortando a execução do script"
        exit 1
    fi
}

verificar_usuario_correto
instalar_repositorio_epel
instalar_dependencias_comuns
instalar_java11
instalar_elasticsearch7
remover_versoes_antigas_do_docker
instalar_docker
instala_cortex
configurando_secretkey_cortex
configurar_analizers_e_responders
iniciar_service_cortex
abrir_porta_no_firewall
configurar_nftables
verificar_servicos