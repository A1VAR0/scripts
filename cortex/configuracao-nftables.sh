#/bin/bash
#Receber o nome do serviço  a ser configurado
read -p 'Informe serviço que deseja configurar: ' SERVICE
echo "Você informou o serviço: $SERVICE"
read -p 'Informe qual a porta padrão deste serviço: ' PORT
echo "Você informou a porta: ${PORT}"
#Criar table do tipo inet
nft add table inet ${SERVICE}_table
#Listar tables atuais
echo "tables atuais: "
nft list tables
#criar uma nova chain
nft add chain inet ${SERVICE}_table ${SERVICE}_chain '{ type filter hook input priority 0 ; policy accept ; }'
#Permitir tráfico na interface de loopback
nft add rule inet ${SERVICE}_table ${SERVICE}_chain iif lo accept
nft add rule inet ${SERVICE}_table ${SERVICE}_chain oif lo accept
##Redirecionamento
#Criando chain específica para essa finalidade
nft -- add chain inet ${SERVICE}_table redirecionar_chain { type nat hook prerouting priority -100 \; }
#Redirecionar da porta 80 para a PORT
nft add rule inet ${SERVICE}_table redirecionar_chain tcp dport 80 redirect to :${PORT}
#Redirecionar porta NEW_PORT_SSH para pora porta ssh 22
read -p 'Informe qual porta deve atender o SSH: ' NEW_PORT_SSH
sudo semanage port -a -t ssh_port_t -p tcp NEW_PORT_SSH
#Aceitar conexões na porta 80
nft add rule inet ${SERVICE}_table ${SERVICE}_chain tcp dport 80 accept
#Listar chains atuais
nft list tables
#Listar regras atuais
nft list ruleset
#Fazendo com que as regras atuais sejam persistidas mesmo após reiniciar o sistema
#ou o nftables.service. Obs esse arquivo irá sobreescrever o arquivo destino
nft list ruleset > /etc/sysconfig/nftables.conf