#/bin/bash
echo "Instalando EPEL"
dnf install -y epel-release
crb enable
echo "Verificando se o EPEL foi instalado"
cat -n /etc/yum.repos.d/Rocky-PowerTools.repo
echo "Instalando as depenências elencadas no tutorial"
dnf install -y pkg-install gnupg chkconfig python3-pip git 

echo "Instalando wget pois não vem por padrão na distro" 
dnf install wget -y

sudo rpm --import https://yum.corretto.aws/corretto.key  &> /dev/null
wget -qO-  https://yum.corretto.aws/corretto.repo | sudo tee -a /etc/yum.repos.d/corretto.repo
dnf install -y java-1.11.0-amazon-corretto-devel &> /dev/null
echo JAVA_HOME="/usr/lib/jvm/java-11-amazon-corretto" |sudo tee -a /etc/environment
export JAVA_HOME="/usr/lib/jvm/java-11-amazon-corretto"

#Instalação do Cassandra DB
rpm --import https://downloads.apache.org/cassandra/KEYS

REPO_CASSANDRA_TEXT="[cassandra]
name=Apache Cassandra
baseurl=https://redhat.cassandra.apache.org/40x/
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://downloads.apache.org/cassandra/KEYS"

REPO_CASSANDRA_FILE="/etc/yum.repos.d/cassandra.repo"


echo "Configurando o repositório" 
echo "$REPO_CASSANDRA_TEXT" > $REPO_CASSANDRA_FILE
echo "Instalando cassandra"
sudo dnf install -y cassandra

echo "Configurando o acesso do cliente do cassandra"
CASSANDRA_CONFIG="/etc/cassandra/default.conf/cassandra.yaml"
IP=$(hostname -I)
sed -e 's/rpc_address: localhost/rpc_address: 0.0.0.0/'  \
    -e "s/# broadcast_rpc_address: \[node\-ip\]/broadcast_rpc_address: $IP/" \
    -e "s/listen_address: localhost/listen_address: $IP/" \
    -e "s/ \- seeds: \"127.0.0.1:7000\"/ \- seeds: \"$IP\"/" $CASSANDRA_CONFIG > /etc/cassandra/default.conf/cassandra.yaml


echo "Executando o serviço cassandro e habilitanto para iniciar juntamente com o sistema"
sudo systemctl daemon-reload
sudo service cassandra start
sudo systemctl enable cassandra

echo "Instalando python36"
#necessário para o comando 
sudo dnf install -y python36

function abrir_porta_no_firewall(){
    echo "Abrindo porta $1"
    firewall-cmd --permanent --zone=public --add-port=$1/tcp
    echo "Reiniciando o firewalld"
    sudo systemctl restart firewalld.servicesyste
    echo "Verificando regras do firewall"
    firewall-cmd --list-all
}

abrir_porta_no_firewall 9042
#Após a instalação
#edite o arquivo /etc/cassandra/default.conf/cassandra.yaml
#configure authenticator: AllowAllAuthenticator para authenticator: PasswordAuthenticator
# create a new superuser
#logue com o usuário padrão
#cqlsh -u cassandra -p cassandra
#crie um novo superusuário para administração
#CREATE ROLE superthehive WITH SUPERUSER = true AND LOGIN = true AND PASSWORD = 'acdf@13*';
#crei um usuário comumpara usar no thehive
#CREATE ROLE thehive WITH PASSWORD = 'treinamento' AND LOGIN = true;
#saia e log com o novo usuario
#exit
#desabilite o antigo superuser
#cqlsh -u superthehive -p acdf@13*
#ALTER ROLE cassandra WITH SUPERUSER = false AND LOGIN = false;