#/bin/bash
function instalar_repositorio_epel(){
    echo "Instalando EPEL"
    dnf install -y epel-release
    crb enable
    echo "Verificando se o EPEL foi instalado"
    cat -n /etc/yum.repos.d/Rocky-PowerTools.repo
}

function instalar_dependencias_comuns(){
    echo "Instalando as depenências elencadas no tutorial"
    dnf install -y pkg-install gnupg chkconfig python3-pip git 
    echo "Instalando wget pois não vem por padrão na distro" 
    dnf install -y wget
}
 
function instalar_java11_corretto(){
    echo "Configurando a chave do java da Amazon"
    sudo rpm --import https://yum.corretto.aws/corretto.key  &> /dev/null
    echo "Configurando o repositório do java da Amazon"
    wget -qO-  https://yum.corretto.aws/corretto.repo | sudo tee -a /etc/yum.repos.d/corretto.repo
    echo "Instalando o java da amazon"
    yum install -y java-11-amazon-corretto-devel &> /dev/null
    echo "configurando a variavél JAVA_HOME"
    echo JAVA_HOME="/usr/lib/jvm/java-11-amazon-corretto" |sudo tee -a /etc/environment
    export JAVA_HOME="/usr/lib/jvm/java-11-amazon-corretto"
}

REPO_CASSANDRA_FILE="/etc/yum.repos.d/cassandra.repo"
REPO_CASSANDRA_TEXT="[cassandra]
name=Apache Cassandra
baseurl=https://redhat.cassandra.apache.org/40x/
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://downloads.apache.org/cassandra/KEYS"

#Instalação do Cassandra DB
function instalar_cassandra(){
    echo "importando a chave do repositório do cassandra"
    rpm --import https://downloads.apache.org/cassandra/KEYS
    echo "Configurando o repositório" 
    echo "$REPO_CASSANDRA_TEXT" > $REPO_CASSANDRA_FILE
    echo "Instalando cassandra"
    sudo yum install -y cassandra
    echo "Executando o serviço cassandra e habilitanto para iniciar juntamente com o sistema"
    sudo systemctl daemon-reload
    sudo systemctl start cassandra
    sudo systemctl enable cassandra
}

REPO_ELASTICSEARCH_FILE=/etc/yum.repos.d/elasticsearch.repo
REPO_ELASTICSEARCH_TEXT="[elasticsearch]
name=Elasticsearch repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=0
autorefresh=1
type=rpm-md"

ELASTICSEARCH_YML_TEXT='http.host: 127.0.0.1
transport.host: 127.0.0.1
cluster.name: hive
thread_pool.search.queue_size: 100000
path.logs: "/var/log/elasticsearch"
path.data: "/var/lib/elasticsearch"
xpack.security.enabled: false
script.allowed_types: "inline,stored"'
ELASTICSEARCH_YML_FILE=/etc/elasticsearch/elasticsearch.yml

JVM_OPTIONS_FILE=/etc/elasticsearch/jvm.options.d/jvm.options
JVM_OPTIONS_TEXT="-Dlog4j2.formatMsgNoLookups=true
-Xms1g
-Xmx1g"

function instalar_elasticsearch7(){
    echo "Configurando o repositório" 
    echo "$REPO_ELASTICSEARCH_TEXT" > $REPO_ELASTICSEARCH_FILE
    echo "Instalando elasticsearch"
    sudo yum install -y --enablerepo=elasticsearch elasticsearch
    echo "Configurando o elasticsearch"  
    echo "$ELASTICSEARCH_YML_TEXT" > $ELASTICSEARCH_YML_FILE
    cat $ELASTICSEARCH_YML_FILE
    echo "Elasticsearch configurado"
    echo "Configurando a JVM"  
    echo "$JVM_OPTIONS_TEXT" > $JVM_OPTIONS_FILE
    echo "Elasticsearch configurado"
    echo "Configurando as permições da pasta /etc/elasticsearch"
    chown -R elasticsearch:elasticsearch /etc/elasticsearch
    echo "verificando as permições da pasta /etc/elasticsearch"
    ls -la /etc/elasticsearch
    echo "Iniciando o elasticsearch"
    systemctl start elasticsearch.service
    echo "Habilitanto o elasticsearch iniciar junto com o sistema"
    systemctl enable elasticsearch.service
    echo "Verificando se o elasticsearch está iniciado"
    systemctl status elasticsearch.service
}


REPO_STRANGEBEE_FILE=/etc/yum.repos.d/strangebee.repo
REPO_STRANGEBEE_TEXT="[thehive]
enabled=1
priority=1
name=StrangeBee RPM repository
baseurl=https://rpm.strangebee.com/thehive-5.2/noarch
gpgkey=https://archives.strangebee.com/keys/strangebee.gpg
gpgcheck=1"

function instalar_thehive(){
    echo "Criando /opt/thp/thehive/files para armazenamento local"
    sudo mkdir -p /opt/thp/thehive/files
    echo "Configurando as permissões da pasta "
    chown -R thehive:thehive /opt/thp/thehive/files
    echo "Verificando as permissões de /opt/thp/thehive/files"
    ls -la /opt/thp/thehive/files
    echo "Adicionando chave GPG do strangebee"
    sudo rpm --import https://archives.strangebee.com/keys/strangebee.gpg 
    echo "Verificando a existência do $REPO_STRANGEBEE_FILE"           
    echo "Configurando o repositório" 
    echo "$REPO_STRANGEBEE_TEXT" > $REPO_STRANGEBEE_FILE
    echo "Instalando O Thehive"
    sudo dnf install -y thehive 
    echo "Configurando as permissões da pasta do thehive"
    sudo chown -R thehive:thehive /opt/thp/thehive/files
    echo "Iniciando o thehive e condfigurando para iniciar junto com o sistema"
    sudo systemctl start thehive
    sudo systemctl enable thehive
}

function abrir_porta_no_firewall(){
    echo "Abrindo porta $1"
    firewall-cmd --permanent --zone=public --add-port=$1/tcp
    echo "Reiniciando o firewalld"
    sudo systemctl restart firewalld.service
    echo "Verificando regras do firewall"
    firewall-cmd --list-all
}

instalar_repositorio_epel
instalar_dependencias_comuns
instalar_java11_corretto
instalar_cassandra
instalar_elasticsearch7
instalar_thehive
abrir_porta_no_firewall 9000
echo "Verifique na lista a seguir se estão disponíveis o elasticsearch, cassandra e thehive"
sudo systemctl list-unit-files --type=service --state=running 
echo "O serviço thehive está disponivel em $(hostname -I):9000"
echo "O usuário padrão é admin@thehive.local e senha é secret" 