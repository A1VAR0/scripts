#/bin/bash
echo "Instalando EPEL"
dnf install epel-release
crb enable
echo "Verificando se o EPEL foi instalado"
cat -n /etc/yum.repos.d/Rocky-PowerTools.repo
echo "Instalando as depenências elencadas no tutorial"
yum install pkg-install gnupg chkconfig python3-pip git 

echo "Instalando wget pois não vem por padrão na distro" 
dnf install wget -y
echo "Configurando a chave do java da Amazon"
sudo rpm --import https://yum.corretto.aws/corretto.key  &> /dev/null
echo "Configurando o repositório do java da Amazon"
wget -qO-  https://yum.corretto.aws/corretto.repo | sudo tee -a /etc/yum.repos.d/corretto.repo
echo "Instalando o java da amazon"
yum install java-1.11.0-amazon-corretto-devel &> /dev/null
echo "configurando a variavél JAVA_HOME"
echo JAVA_HOME="/usr/lib/jvm/java-11-amazon-corretto" |sudo tee -a /etc/environment
export JAVA_HOME="/usr/lib/jvm/java-11-amazon-corretto"

REPO_ELASTICSEARCH_FILE=/etc/yum.repos.d/elasticsearch.repo
REPO_ELASTICSEARCH_TEXT="[elasticsearch]
name=Elasticsearch repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=0
autorefresh=1
type=rpm-md"

echo "Verificando a existência do $REPO_ELASTICSEARCH_FILE"           
echo "Configurando o repositório" 
echo "$REPO_ELASTICSEARCH_TEXT" > $REPO_ELASTICSEARCH_FILE
echo "Instalando elasticsearch"
sudo yum install -y --enablerepo=elasticsearch elasticsearch

ELASTICSEARCH_YML_TEXT='http.host: 127.0.0.1
transport.host: 127.0.0.1
cluster.name: hive
thread_pool.search.queue_size: 100000
path.logs: "/var/log/elasticsearch"
path.data: "/var/lib/elasticsearch"
xpack.security.enabled: false
script.allowed_types: "inline,stored"
discovery.type: single-node'
ELASTICSEARCH_YML_FILE=/etc/elasticsearch/elasticsearch.yml

#discovery.type: single-node é para funcionamemto quando instalado aparte         

echo "Configurando o elasticsearch.yml"  
echo "$ELASTICSEARCH_YML_TEXT" > $ELASTICSEARCH_YML_FILE
echo "Elasticsearch configurado"


JVM_OPTIONS_FILE=/etc/elasticsearch/jvm.options.d/jvm.options
JVM_OPTIONS_TEXT="-Dlog4j2.formatMsgNoLookups=true
-Xms512m
-Xmx512m"


echo "Configurando o elasticsearch"  
echo "$JVM_OPTIONS_TEXT" > $JVM_OPTIONS_FILE
echo "Elasticsearch configurado"


echo "Configurando as permições da pasta /etc/elasticsearch"
chown -R elasticsearch:elasticsearch /etc/elasticsearch
echo "verificando as permições da pasta /etc/elasticsearch"
ls -la /etc/elasticsearch

echo "Iniciando o elasticsearch"
systemctl start elasticsearch.service
echo "Habilitanto o elasticsearch iniciar junto com o sistema"
systemctl enable elasticsearch.service
echo "Verificando se o elasticsearch está iniciado"
systemctl status elasticsearch.service