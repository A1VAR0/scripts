mkdir /root/ca 
cd /root/ca
CERT="thehive-server-chained-cert.pem"
openssl req -new -x509 -newkey rsa:2048 -keyout cakey.pem -out $CERT -days 3650
#verificar se consegue ver a chave fornecendo a senha
openssl rsa -in cakey.pem -noout -text

openssl req -newkey rsa:4096 \
            -x509 \
            -sha256 \
            -days 3650 \
            -nodes \
            -out example.crt \
            -keyout example.key \
            -subj "/C=BR/ST=Piaui/L=Teresina/O=Security/OU=IT Department/CN=thehive"
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt
