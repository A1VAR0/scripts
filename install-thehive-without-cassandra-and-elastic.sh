#/bin/bash
function instalar_repositorio_epel(){
    echo "Instalando EPEL"
    dnf install -y epel-release
    crb enable
    echo "Verificando se o EPEL foi instalado"
    cat -n /etc/yum.repos.d/Rocky-PowerTools.repo
}

instalar_repositorio_epel 

instalar_dependencias_comuns(){
    echo "Instalando as depenências elencadas no tutorial"
    dnf install -y pkg-install gnupg chkconfig python3-pip git 
    echo "Instalando wget pois não vem por padrão na distro" 
    dnf install wget -y
}

instalar_dependencias_comuns

function instalar_java11_corretto(){
    echo "Configurando a chave do java da Amazon"
    sudo rpm --import https://yum.corretto.aws/corretto.key  &> /dev/null
    echo "Configurando o repositório do java da Amazon"
    wget -qO-  https://yum.corretto.aws/corretto.repo | sudo tee -a /etc/yum.repos.d/corretto.repo
    echo "Instalando o java da amazon"
    dnf install java-11-amazon-corretto-devel &> /dev/null
    echo "configurando a variavél JAVA_HOME"
    echo JAVA_HOME="/usr/lib/jvm/java-11-amazon-corretto" |sudo tee -a /etc/environment
    export JAVA_HOME="/usr/lib/jvm/java-11-amazon-corretto"
}

function instalar_thehive(){
    echo "Criando /opt/thp/thehive/files para armazenamento local"
    sudo mkdir -p /opt/thp/thehive/files
    echo "Configurando as permissões da pasta "
    chown -R thehive:thehive /opt/thp/thehive/files
    echo "Verificando as permissões de /opt/thp/thehive/files"
    ls -la /opt/thp/thehive/files

    echo "Adicionando chave GPG do strangebee"
    sudo rpm --import https://archives.strangebee.com/keys/strangebee.gpg 


    REPO_STRANGEBEE_FILE=/etc/yum.repos.d/strangebee.repo
    REPO_STRANGEBEE_TEXT="[thehive]
    enabled=1
    priority=1
    name=StrangeBee RPM repository
    baseurl=https://rpm.strangebee.com/thehive-5.2/noarch
    gpgkey=https://archives.strangebee.com/keys/strangebee.gpg
    gpgcheck=1"
    echo "Verificando a existência do $REPO_STRANGEBEE_FILE"           

    echo "Configurando o repositório" 
    echo "$REPO_STRANGEBEE_TEXT" > $REPO_STRANGEBEE_FILE
    echo "Instalando O Thehive"
    sudo dnf install -y thehive


    echo "Configurando as permissões da pasta do thehive"
    sudo chown -R thehive:thehive /opt/thp/thehive/files
}

instalar_thehive

#echo "Iniciando o thehive e condfigurando para iniciar junto com o sistema"
#sudo systemctl start thehive
#sudo systemctl enable thehive

function abrir_porta_no_firewall(){
    echo "Abrindo porta $1"
    firewall-cmd --permanent --zone=public --add-port=$1/tcp
    echo "Reiniciando o firewalld"
    sudo systemctl restart firewalld.servicesyste
    echo "Verificando regras do firewall"
    firewall-cmd --list-all
}

abrir_porta_no_firewall 9000

echo "Verifique na lista a seguir se estão disponíveis o elasticsearch, cassandra e thehive"
sudo systemctl list-units --type=service --state=active
echo "O serviço thehive está disponivel em $(hostname -I):9000"
echo "O usuário padrão é admin@thehive.local e senha é secret" 

